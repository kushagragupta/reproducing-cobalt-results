# Reproducing COBALT results

Download and install ns-3-dev:
https://gitlab.com/nsnam/ns-3-dev

Names of the scripts:
1. Light-Traffic.cc
2. Heavy-Traffic.cc
3. Mix-Traffic.cc

Put these scripts in the scratch directory of your ns-3-dev, and then run them one by one.

**Steps to be followed to run the above scripts**: <br/>
On recent Linux systems, once you have built ns-3 (with examples enabled), it should be easy to run the given scripts with the
following command in the ns-3-dev directory, such as:

- $ ./waf --run scratch/Heavy-Traffic
- $ ./waf --run scratch/Light-Traffic
- $ ./waf --run scratch/Mix-Traffic

After running the following commands, 3 folders namely PlotsHeavy, PlotsLight, PlotsMix should be generated.

**Steps to be followed to reproduce the plots**:
1. Install gnuplot: 
- $ sudo apt install gnuplot
2. Put the "gnuplotScriptQueueSize" file at these 3 locations: ns-3-dev/PlotsHeavy/queueTraces, ns-3-dev/PlotsLight/queueTraces, ns-3-dev/PlotsMix/queueTraces
3. Now, to generate the plots run the following command in queueTraces directory for each of the scripts: 
- $ gnuplot gnuplotScriptQueueSize
